﻿import QtQuick 2.7
import QtQuick.Controls 1.5
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    width: 600
    height: 240
    title: qsTr("样式定制")

    MainForm {
        anchors.fill: parent
    }
}
