﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QMessageBox>
#include<QFileDialog>
#include<QBuffer>
#include<QSqlDatabase>
#include<QSqlTableModel>
#include<QSqlQuery>
#include<QTime>
#include<QPixmap>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void initMainWindow();
    void onTableSelectChange(int row);
    void showCommodityPhoto();
    void loadPreCommodity();
    void onPreNameComboBoxChange();
private slots:
    void on_commodityTableView_clicked(const QModelIndex &index);
    void on_preCategoryComboBox_currentIndexChanged(int index);
    void on_preNameComboBox_currentIndexChanged(int index);
    void on_preCountSpinBox_valueChanged(int arg1);
    void on_preSellPushButton_clicked();
    void on_prePlaceOrderPushButton_clicked();
    void on_newUploadPushButton_clicked();
    void on_newPutinStorePushButton_clicked();
    void on_newClearancePushButton_clicked();

private:
    Ui::MainWindow *ui;
    QImage myPicImg;
    QSqlTableModel *commodity_model;
    QString myMemberID;
    bool myOrdered;
    int myOrderID;
    float myPaySum;
};

static bool createMySqlConn()
{
    QSqlDatabase sqldb=QSqlDatabase::addDatabase("QMYSQL");
    sqldb.setHostName("localhost");
    sqldb.setDatabaseName("emarket");
    sqldb.setUserName("root");
    sqldb.setPassword("bsqtzyd1988");
    if(!sqldb.open())
    {
        QMessageBox::critical(0,u8"后台数据库连接失败",u8"无法创建连接！请检查排除故障后重启程序。",QMessageBox::Cancel);
        return  false;
    }
    return  true;
}
#endif // MAINWINDOW_H
