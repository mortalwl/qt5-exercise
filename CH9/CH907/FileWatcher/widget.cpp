﻿#include "widget.h"
#include<QDir>
#include<QMessageBox>
#include<QApplication>
#include<QVBoxLayout>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QStringList args=qApp->arguments();
    QString path;
    if(args.count()>1)
    {
        path=args[1];
    }
    else {
        path=QDir::currentPath();
    }
    pathLabel=new QLabel;
    pathLabel->setText(u8"监视的目录："+path);
    QVBoxLayout *mainLayout=new QVBoxLayout(this);
    mainLayout->addWidget(pathLabel);
    fsWatcher.addPath(path);
    connect(&fsWatcher,SIGNAL(directoryChanged(QString)),this,SLOT(directoryChanged(QString)));
}

Widget::~Widget()
{

}

void Widget::directoryChanged(QString path)
{
    QMessageBox::information(NULL,u8"目录发生变化",path);
}
