﻿#ifndef MYCHILDWND_H
#define MYCHILDWND_H

#include<QWidget>
#include<QTextEdit>
#include<QFileInfo>
#include<QFileDialog>
#include<QTextCodec>
#include<QFileDialog>
#include<QMessageBox>
#include<QCloseEvent>
#include<QtWidgets>
class MyChildWnd : public QTextEdit
{
    Q_OBJECT
public:
    MyChildWnd();
    QString myCurDocPath;
    void newDoc();
    QString getCurDocName();
    bool loadDoc(const QString &docName);
    bool saveDoc();
    bool saveAsDoc();
    bool saveDocOpt(QString docName);
    void setFormatOnSelectedWord(const QTextCharFormat &fmt);
    void setAlgignOfDocumentText(int aligntype);
    void setParaStyle(int pstyle);
protected:
   void closeEvent(QCloseEvent *event);
private slots:
    void docBeModified();
private:
    bool beSaved;
    void setCurDoc(const QString &docName);
    bool promptSave();
};


#endif // MYCHILDWND_H
