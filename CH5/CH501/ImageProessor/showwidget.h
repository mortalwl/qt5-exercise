﻿#ifndef SHOWIDGET_H
#define SHOWIDGET_H

#include<QWidget>
#include<QLabel>
#include<QTextEdit>
#include<QImage>
class ShowWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ShowWidget(QWidget *parent = nullptr);
    QImage img;
    QLabel *imageLabel;
    QTextEdit *text;
signals:

public slots:
};

#endif // SHOWIDGET_H
