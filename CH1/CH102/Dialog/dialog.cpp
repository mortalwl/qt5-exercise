﻿#include "dialog.h"
#include "qtextcodec.h"
#include <QGridLayout>
#pragma execution_character_set("UTF-8")
const static double PI=3.14159;
Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
    label1=new QLabel(this);
    label1->setText(QString("请输入圆的半经"));
    lineEdit=new QLineEdit(this);
    label2=new QLabel(this);
    button=new QPushButton(this);
    button->setText(QString("显示对应原的面积"));
    QGridLayout *mainLayout=new QGridLayout;
    mainLayout->addWidget(label1,0,0);
    mainLayout->addWidget(lineEdit,0,1);
    mainLayout->addWidget(label2,1,0);
    mainLayout->addWidget(button,1,1);

    connect(button,SIGNAL(clicked()),this,SLOT(showArea()));
    connect(lineEdit,SIGNAL(textChanged(QString)),this,SLOT(showArea()));
}

Dialog::~Dialog()
{

}

void Dialog::showArea()
{
    bool ok;
    QString tempStr;
    QString valueStr= lineEdit->text();
    int valueInt=valueStr.toInt(&ok);
    double area=valueInt*valueInt*PI;
    label2->setText((tempStr.setNum(area)));
}
