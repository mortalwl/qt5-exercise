#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

class Palette : public QDialog
{
    Q_OBJECT

public:
    Palette(QWidget *parent = 0);
    ~Palette();
};

#endif // DIALOG_H
